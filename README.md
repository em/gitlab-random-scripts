# Gitlab Random Scripts
A brief documentation of how to configure and use.
## Install python-gitlab
It's necessary to install in your local the python-gitlab package

```
sudo apt install python-gitlab
```
## Configure Gitlab instance
_[Official documentation](https://python-gitlab.readthedocs.io/en/stable/cli-usage.html)_

### Configuration files
Gitlab looks up 3 configuration files by default:
- An environment variable that contains the path to a configuration file.
The ```PYTHON_GITLAB_CFG``` environment variable
- System-wide configuration file
```/etc/python-gitlab.cfg```
- User configuration file
```~/.python-gitlab.cfg```

The configuration file uses the INI format. It contains at least a [global] section, and a specific section for each GitLab server. For example:
```
[global]
default = gitlab1
ssl_verify = true
timeout = 5

[gitlab1]
url = https://some.whe.re
private_token = vTbFeqJYCY3sibBP7BZM
api_version = 4

[gitlab2]
url = http://else.whe.re:8080
private_token = helper: path/to/helper.sh
timeout = 1
```
## Execute
### gitlab-jobs-list

Run using the configuration file:

```./gitlab-jobs-list.py --gitlab-instance gitlab1 --tags "lightweight or uml or empty" --created-after 2024-02-22T00:00Z --json | tee gitlab-jobs-lightweight-uml-2024-02-22.jsonlines```

Run declaring the gitlab server and API token directly:

```./gitlab-jobs-list.py --gitlab-server-url https://gitlab-instance.com/ --gitlab-api-token XXXXXXXX --created-after 2024-02-22T00:00Z --json | tee gitlab-jobs-lightweight-uml-2024-02-22.jsonlines```