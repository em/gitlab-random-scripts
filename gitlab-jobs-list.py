#!/usr/bin/env python3
# /// script
# dependencies = [
#   "python-gitlab",
# ]
# ///

import argparse
import ast
import datetime
import fnmatch
import json
import logging
import re
import textwrap
from collections import namedtuple
from typing import Any, Optional

import gitlab

QUERY = """
query getAllJobs($after: String, $first: Int, $statuses: [CiJobStatus!], $runnerTypes: [CiRunnerType!]) {
  jobs(
    after: $after
    first: $first
    statuses: $statuses
    runnerTypes: $runnerTypes
    ) {
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
      startCursor
      __typename
    }
    nodes {
      id
      runner {
        description
        adminUrl
        runnerType
        __typename
      }
      webPath
      createdAt
      allowFailure
      status
      scheduledAt
      manualJob
      triggered
      createdByTag
      refName
      refPath
      tags
      shortSha
      commitPath
      pipeline {
        id
        project {
          id
          fullPath
          webUrl
          __typename
        }
        path
        user {
          username
          webPath
          __typename
        }
        __typename
      }
      stage {
        name
        __typename
      }
      name
      duration
      queuedDuration
      finishedAt
      retryable
      playable
      cancelable
      active
      stuck
      userPermissions {
        readBuild
        readJobArtifacts
        updateBuild
        __typename
      }
      __typename
    }
    __typename
  }
}
"""

ICONS = {
    "SUCCESS": "✅",
    "SKIPPED": "»",
    "FAILED": "🛑",
    "PENDING": "⚇ ",
    "RUNNING": "▶️ ",
    "MANUAL": "⚙️ ",
    "CANCELED": "⊗ ",
    "CREATED": "⊙ ",
    "PREPARING": "🛠",
    "SCHEDULED": "⏲ ",
    "WAITING_FOR_CALLBACK": "↩",
    "WAITING_FOR_RESOURCE": "🏷️",
}


# from https://github.com/em-/python-ast-expression-matcher
class ExpressionMatcher:
    class Transformer(ast.NodeTransformer):
        def __init__(self, expression: str | None) -> None:
            self.expression = expression

        def error(self, msg: str, lineno: int, col_offset: int):
            raise SyntaxError(
                msg,
                (None, lineno, col_offset + 1, self.expression),
            )

        def generic_visit(self, node: ast.AST) -> ast.AST:
            items_var = "items"

            match node:
                case ast.Name(id=id, ctx=ast.Load()):
                    return ast.Compare(
                        left=ast.Constant(value=id),
                        ops=[ast.In()],
                        comparators=[ast.Name(id=items_var, ctx=ast.Load())],
                    )
                case ast.Constant(value=value):
                    return ast.Compare(
                        left=ast.Constant(value=str(value)),
                        ops=[ast.In()],
                        comparators=[ast.Name(id=items_var, ctx=ast.Load())],
                    )
                case ast.Call(func=ast.Name(id="empty"), args=[], keywords=[]):
                    return ast.UnaryOp(
                        op=ast.Not(),
                        operand=ast.Name(id=items_var, ctx=ast.Load()),
                    )
                case ast.Call(func=ast.Name(id="anything"), args=[], keywords=[]):
                    return ast.Constant(value=True)
                case (
                    ast.Expression()
                    | ast.BoolOp(op=ast.Or() | ast.And())
                    | ast.Or()
                    | ast.And()
                    | ast.UnaryOp(op=ast.Not())
                    | ast.Not()
                ):
                    pass
                case ast.Call(func=ast.Name(id="empty" | "anything" as func)):
                    # point the error to the argument list
                    self.error(
                        f"invalid syntax, {func}() does not accept any argument",
                        node.func.end_lineno or node.lineno,
                        node.func.end_col_offset or node.col_offset,
                    )
                case ast.Call(func=ast.Name()):
                    self.error(
                        "invalid syntax, unknown function",
                        node.lineno,
                        node.col_offset,
                    )
                case ast.BinOp(left=ast.Name()):
                    # point the error to the operator
                    self.error(
                        "invalid syntax, unsupported operation",
                        node.left.end_lineno or node.lineno,
                        node.left.end_col_offset or node.col_offset,
                    )
                case _:
                    self.error("invalid syntax", node.lineno, node.col_offset + 1)
            return super().generic_visit(node)

    def __init__(self, expression: str | None) -> None:
        """Inizialize the ExpressionMatcher

        >>> ast.unparse(ExpressionMatcher("foo and bar").ast)
        "'foo' in items and 'bar' in items"
        >>> ast.unparse(ExpressionMatcher("foo or empty()").ast)
        "'foo' in items or not items"
        >>> ast.unparse(ExpressionMatcher(None).ast)
        'True'
        >>> ExpressionMatcher("foo + 1")
        Traceback (most recent call last):
          ...
          File "<string>", line 1
            foo + 1
               ^
        SyntaxError: invalid syntax, unsupported operation
        >>> ExpressionMatcher("foo()")
        Traceback (most recent call last):
          ...
          File "<string>", line 1
            foo()
            ^
        SyntaxError: invalid syntax, unknown function
        >>> ExpressionMatcher("empty(1)")
        Traceback (most recent call last):
          ...
          File "<string>", line 1
            empty(1)
                 ^
        SyntaxError: invalid syntax, empty() does not accept any argument
        """
        if not expression:
            expression = "anything()"
        parsed = ast.parse(expression, mode="eval")
        transformed = self.Transformer(expression).visit(parsed)
        ast.fix_missing_locations(transformed)
        compiled = compile(transformed, filename="<ast>", mode="eval")

        def matches(items):
            match = eval(compiled, {}, {"items": items})
            return match

        self.expression = expression
        self.ast = transformed
        self.matches = matches

    def __call__(self, items) -> bool:
        """Test if the expression matches when applied on the `items` collection

        >>> matches = ExpressionMatcher("foo and bar")
        >>> matches([])
        False
        >>> matches({"foo"})
        False
        >>> matches(["foo", "bar"])
        True
        >>> matches("foobarbaz")
        True
        >>> matches = ExpressionMatcher("foo or empty()")
        >>> matches({"foo"})
        True
        >>> matches([])
        True
        >>> matches(["bar"])
        False
        """
        return self.matches(items)

    def __repr__(self) -> str:
        """String representation

        >>> repr(ExpressionMatcher("foo and bar"))
        "ExpressionMatcher('foo and bar')"
        >>> repr(ExpressionMatcher(""))
        "ExpressionMatcher('anything()')"
        >>> repr(ExpressionMatcher(None))
        "ExpressionMatcher('anything()')"
        """
        return f"{self.__class__.__qualname__}('{self.expression}')"


def dict_to_object(d: dict[str, Any]):
    typename = d.pop("__typename", "DictObject")
    if typename == "CiJob":
        d["trace_snippet"] = None
    properties = list(sorted(d.keys()))
    typehash = "".join([typename, *properties])
    namedtuples = dict_to_object.__dict__.setdefault("namedtuples", {})
    if typehash not in namedtuples:
        namedtuples[typehash] = namedtuple(typename, d.keys())
    return namedtuples[typehash](*d.values())


def recursive_dict_to_object(data: Any):
    if isinstance(data, list):
        data = [recursive_dict_to_object(i) for i in data]
    if isinstance(data, dict):
        data = {k: recursive_dict_to_object(v) for k, v in data.items()}
        data = dict_to_object(data)
    return data


def clean_escapes(s):
    it = iter(s)
    for b in it:
        if b == "\x1b":  # ESC
            # eat the escaped character
            b = next(it, None)
            if b == "[":
                # eat whole CSI sequences
                while True:
                    b = next(it, None)
                    if b is None or "\x40" <= b <= "\x7E":
                        break
            continue
        yield b


def clean_string(s):
    chars = clean_escapes(s)
    safe = ["\n", "\t", " "]
    return "".join(c for c in chars if c in safe or c.isprintable())


def printable_link(uri: str, label: Optional[str] = None, **kwargs):
    # see https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda
    label = label if label else uri
    params = ",".join(f"{k}={v}" for k, v in kwargs.items())
    # OSC 8 ; params ; URI ST <name> OSC 8 ;; ST
    ret = f"\033]8;{params};{uri}\033\\{label}\033]8;;\033\\"
    return ret


def render_duration(duration):
    if not duration:
        return ""
    duration = datetime.timedelta(seconds=int(duration))
    return str(duration)


def gid_to_id(gid: str, typeid: str) -> int:
    prefix = f"gid://gitlab/{typeid}/"
    assert gid.startswith(prefix)
    ret = gid.rsplit("/", 1)[-1]
    return int(ret)


class Lister:
    def __init__(self):
        self.gl = None
        self.projects = None
        self.pipelines = None

    def connect(self, gitlab_instance, gitlab_server_url, gitlab_api_token):
        if gitlab_server_url:
            logging.info(f'Connecting to the "{gitlab_server_url}" instance')
            self.gl = gitlab.Gitlab(gitlab_server_url, private_token=gitlab_api_token)
        else:
            logging.info(f'Connecting to the "{gitlab_instance}" configured instance')
            self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()

    def fetch_jobs_page(self, **kwargs):
        data = {"query": QUERY, "variables": kwargs}
        response = self.gl.http_post(self.gl.url + "/api/graphql", post_data=data)
        if isinstance(response, dict):
            results = recursive_dict_to_object(response)
        else:
            # python-gitlab <= 3.x
            results = response.json(object_hook=dict_to_object)
        if hasattr(results, "errors"):
            raise Exception(results.errors)
        return results

    def filter_job(
        self,
        job,
        projects: Optional[str],
        refs: Optional[str],
        tags_match: ExpressionMatcher,
        created_before: datetime.datetime,
        created_after: datetime.datetime,
    ):
        if projects and not fnmatch.fnmatch(job.pipeline.project.fullPath, projects):
            return False
        if refs and not fnmatch.fnmatch(job.refName, refs):
            return False
        created_at = datetime.datetime.fromisoformat(job.createdAt)
        if created_before and created_at >= created_before:
            return False
        if created_after and created_at < created_after:
            return None  # signal that fetching should stop
        if not tags_match(job.tags):
            return False
        return True

    def fetch_jobs(
        self,
        projects: Optional[str],
        refs: Optional[str],
        statuses: Optional[list[str]],
        tags_match: ExpressionMatcher,
        created_before: datetime.datetime,
        created_after: datetime.datetime,
        trace_chunk: Optional[int],
        trace_grep: Optional[re.Pattern],
    ):
        has_next = True
        cursor: Optional[str] = None
        while has_next:
            r = self.fetch_jobs_page(after=cursor, statuses=statuses)
            data = r.data.jobs
            page_info = data.pageInfo
            cursor = page_info.endCursor
            has_next = page_info.hasNextPage
            jobs = data.nodes
            for job in jobs:
                keep = self.filter_job(
                    job, projects, refs, tags_match, created_before, created_after
                )
                if keep is None:  # stop the iteration
                    return
                if keep:
                    if trace_chunk:
                        pid = gid_to_id(job.pipeline.project.id, "Project")
                        p = self.gl.projects.get(pid, lazy=True)
                        jid = gid_to_id(job.id, "Ci::Build")
                        j = p.jobs.get(jid, lazy=True)
                        trace = next(
                            j.trace(chunk_size=trace_chunk, iterator=True),
                            None,
                        )
                        if trace:
                            trace = trace.decode("utf-8", errors="replace")
                            trace = clean_string(trace)
                            job = job._replace(trace_snippet=trace)
                        if trace_grep and not trace_grep.search(trace or ""):
                            continue
                    yield job

    def render_job(self, job):
        url = self.gl.url + job.webPath
        label = job.webPath.lstrip("/").replace("/-/jobs/", " #")
        created_at = datetime.datetime.fromisoformat(job.createdAt)
        created_at_local_tz = created_at.astimezone().isoformat()
        ret = " ".join(
            [
                created_at_local_tz.replace("T", " ").replace("+", " +"),
                job.status,
                ICONS.get(job.status, ""),
                printable_link(url, label),
                job.refName,
                f"'{job.name}'",
                ",".join(job.tags),
                render_duration(job.queuedDuration),
                "🏁" if job.duration else "",
                render_duration(job.duration),
            ]
        )
        return ret

    def render_job_json(self, job):
        url = self.gl.url + job.webPath
        attrs = [
            "createdAt",
            "status",
            "refName",
            "name",
            "tags",
            "queuedDuration",
            "duration",
        ]
        data = {a: getattr(job, a) for a in attrs}
        data["url"] = url
        data["stage"] = job.stage.name
        return data

    def cancel_job_pipeline(self, job):
        project_path = job.pipeline.project.fullPath
        pipeline_id = gid_to_id(job.pipeline.id, "Ci::Pipeline")
        project = self.gl.projects.get(project_path, lazy=True)
        pipeline = project.pipelines.get(pipeline_id, lazy=True)
        try:
            pipeline.cancel()
            action = "❌ [pipeline canceled]"
        except gitlab.exceptions.GitlabPipelineCancelError as e:
            action = (
                f"❌ [pipeline cancellation failed: {e.response_code} {e.error_message}]"
            )
        return action


def isodatetime(s: str) -> datetime.datetime:
    when = datetime.datetime.fromisoformat(s)
    if not when.tzinfo:
        local_tzinfo = datetime.datetime.now().astimezone().tzinfo
        when = when.replace(tzinfo=local_tzinfo)
    return when


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
        help="do not print informational output",
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance, see https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration",
    )
    parser.add_argument("--gitlab-api-token", type=str, help="the GitLab API token")
    parser.add_argument("--gitlab-server-url", type=str, help="the GitLab instance URL")
    parser.add_argument(
        "--projects",
        type=str,
        help="filter jobs by project matching the specified path glob",
    )
    parser.add_argument(
        "--status",
        action="append",
        choices=[
            "CANCELED",
            "CREATED",
            "FAILED",
            "MANUAL",
            "PENDING",
            "PREPARING",
            "RUNNING",
            "SCHEDULED",
            "SKIPPED",
            "SUCCESS",
            "WAITING_FOR_CALLBACK",
            "WAITING_FOR_RESOURCE",
        ],
        type=str,
        help="filter jobs by status, can be specified multiple times to match any",
    )
    parser.add_argument(
        "--tags",
        type=ExpressionMatcher,
        default=ExpressionMatcher(None),
        help="filter jobs by tag using a boolean expression like "
        + "'foo or (bar and baz) or empty()' ",
    )
    parser.add_argument(
        "--created-before",
        type=isodatetime,
        help="filter jobs created before a specific date",
    )
    parser.add_argument(
        "--created-after",
        type=isodatetime,
        help="filter jobs created after a specific date",
    )
    parser.add_argument(
        "--ref",
        type=str,
        help="filter jobs by ref matching the specified path glob",
    )
    parser.add_argument(
        "--cancel-pipeline",
        action="store_true",
        help="cancel the pipeline of the job",
    )
    parser.add_argument(
        "--json",
        action="store_true",
        help="output JSON",
    )
    parser.add_argument(
        "--trace-chunk",
        type=int,
        metavar="N",
        help="fetch N bytes of trace log and show them",
    )
    parser.add_argument(
        "--trace-grep",
        type=re.compile,
        metavar="REGEX",
        help="match REGEX on the cleaned up trace log chunk",
    )
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel or logging.INFO)

    lister = Lister()
    lister.connect(args.gitlab_instance, args.gitlab_server_url, args.gitlab_api_token)
    jobs = lister.fetch_jobs(
        args.projects,
        args.ref,
        args.status,
        args.tags,
        args.created_before,
        args.created_after,
        args.trace_chunk,
        args.trace_grep,
    )

    for job in jobs:
        if args.json:
            j = lister.render_job_json(job)
            if args.cancel_pipeline:
                action = lister.cancel_job_pipeline(job)
                j["action"] = "canceled"
            print(json.dumps(j))
        else:
            print(lister.render_job(job), "", end="")
            action = ""
            if args.cancel_pipeline:
                action = lister.cancel_job_pipeline(job)
            print(action)
            if args.trace_chunk:
                if job.trace_snippet:
                    trace = textwrap.indent(job.trace_snippet, "  🭰")
                else:
                    trace = "  <empty>"
                print(f"\x1b[2m{trace}\x1b[0m")
